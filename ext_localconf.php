<?php
declare(strict_types=1);

defined('TYPO3') or die();

(function () {
    // XClass for Argument, to add setDataType method
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Extbase\Mvc\Controller\Argument::class] = [
        'className' => \MEDIAESSENZ\FemanagerMailSubscribe\Xclass\Extbase\Mvc\Controller\Argument::class
    ];

    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\In2code\Femanager\Domain\Model\User::class] = [
        'className' => \MEDIAESSENZ\FemanagerMailSubscribe\Domain\Model\User::class
    ];

    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\In2code\Femanager\Controller\NewController::class] = [
        'className' => \MEDIAESSENZ\FemanagerMailSubscribe\Controller\NewController::class
    ];

    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\In2code\Femanager\Controller\EditController::class] = [
        'className' => \MEDIAESSENZ\FemanagerMailSubscribe\Controller\EditController::class
    ];

})();
