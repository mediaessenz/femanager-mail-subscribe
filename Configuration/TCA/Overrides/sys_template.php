<?php
defined('TYPO3') or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'femanager_mail_subscribe',
    'Configuration/TypoScript',
    'femanager mail subscription'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'femanager_mail_subscribe',
    'Configuration/TypoScript/Salutation',
    'femanager mail salutation finisher'
);
