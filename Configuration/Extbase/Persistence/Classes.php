<?php
declare(strict_types = 1);

return [
    \In2code\Femanager\Domain\Model\User::class => [
        'subclasses' => [
            \MEDIAESSENZ\FemanagerMailSubscribe\Domain\Model\User::class
        ]
    ],

    \MEDIAESSENZ\FemanagerMailSubscribe\Domain\Model\User::class => [
        'tableName' => 'fe_users',
        'recordType' => 0
    ],

    \MEDIAESSENZ\FemanagerMailSubscribe\Domain\Model\Category::class => [
        'tableName' => 'sys_category',
        'recordType' => \MEDIAESSENZ\FemanagerMailSubscribe\Domain\Model\Category::class,
    ]
];
