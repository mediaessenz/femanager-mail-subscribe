<?php
declare(strict_types=1);

namespace MEDIAESSENZ\FemanagerMailSubscribe\Finisher;

use MEDIAESSENZ\FemanagerMailSubscribe\Domain\Model\User;
use In2code\Femanager\Finisher\AbstractFinisher;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

class AddSalutationFinisher extends AbstractFinisher
{
    /**
     * @var User
     */
    protected $user;

    /**
     * addSalutationFinisher
     *
     * @return void
     * @throws UnknownObjectException
     */
    public function addSalutationFinisher()
    {
        // get value from configuration
        $genderConfig = $this->configuration['gender'];

        $gender = $this->user->getGender();

        if (!$this->user->getMailSalutation() && array_key_exists($gender, $genderConfig)) {
            $config = $genderConfig[$gender];
            $salutation = $this->configuration['default'] ?? '';
            if (is_array($salutation)) {
                $salutation = $this->contentObject->cObjGetSingle($salutation['_typoScriptNodeValue'], $salutation);
            }
            if ($this->user->getLastName()) {
                $salutationWithName = $config['salutationWithName'] ?? $salutation;
                if (is_array($salutationWithName)) {
                    $salutationWithName = $this->contentObject->cObjGetSingle($salutationWithName['_typoScriptNodeValue'], $salutationWithName);
                }
                $salutation = sprintf($salutationWithName, $this->user->getLastName());
            } else {
                $salutationWithoutName = $config['salutationWithoutName'] ?? $salutation;
                if (is_array($salutationWithoutName)) {
                    $salutationWithoutName = $this->contentObject->cObjGetSingle($salutationWithoutName['_typoScriptNodeValue'], $salutationWithoutName);
                }
                $salutation = $salutationWithoutName;
            }
            $this->user->setMailSalutation($salutation);
            $persistenceManager = GeneralUtility::makeInstance(PersistenceManager::class);
            $persistenceManager->update($this->user);
            $persistenceManager->persistAll();
        }
    }
}
