<?php
namespace MEDIAESSENZ\FemanagerMailSubscribe\Controller;

use JsonException;
use MEDIAESSENZ\FemanagerMailSubscribe\Domain\Repository\CategoryRepository;
use MEDIAESSENZ\FemanagerMailSubscribe\Utility\CategoriesUtility;
use MEDIAESSENZ\FemanagerMailSubscribe\Xclass\Extbase\Mvc\Controller\Argument;
use In2code\Femanager\Domain\Model\User;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class NewController
 */
class NewController extends \In2code\Femanager\Controller\NewController
{
    /**
     * Workaround to avoid php warnings of wrong type hint.
     */
    public function initializeNewAction(): void
    {
        if ($this->arguments->hasArgument('user')) {
            /** @var Argument $user */
            $user = $this->arguments['user'];
            $user->setDataType(\MEDIAESSENZ\FemanagerMailSubscribe\Domain\Model\User::class);
        }
    }

    /**
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("user")
     * @throws JsonException
     */
    public function newAction(User $user = null): ResponseInterface
    {
        $categoryRepository = GeneralUtility::makeInstance(CategoryRepository::class);
        $this->view->assign('categories', CategoriesUtility::filterCategories($categoryRepository->findAll(), $this->settings));
        return parent::newAction();
    }

    /**
     * Workaround to avoid php warnings of wrong type hint.
     */
    public function initializeCreateAction(): void
    {
        if ($this->arguments->hasArgument('user')) {
            /** @var Argument $user */
            $user = $this->arguments['user'];
            $user->setDataType(\MEDIAESSENZ\FemanagerMailSubscribe\Domain\Model\User::class);
        }
    }
}
