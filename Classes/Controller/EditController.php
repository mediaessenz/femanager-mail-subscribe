<?php
namespace MEDIAESSENZ\FemanagerMailSubscribe\Controller;

use MEDIAESSENZ\FemanagerMailSubscribe\Domain\Repository\CategoryRepository;
use MEDIAESSENZ\FemanagerMailSubscribe\Utility\CategoriesUtility;
use MEDIAESSENZ\FemanagerMailSubscribe\Xclass\Extbase\Mvc\Controller\Argument;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class EditController
 */
class EditController extends \In2code\Femanager\Controller\EditController
{
    public function editAction(): ResponseInterface
    {
        $categoryRepository = GeneralUtility::makeInstance(CategoryRepository::class);
        $this->view->assign('categories', CategoriesUtility::filterCategories($categoryRepository->findAll(), $this->settings));
        return parent::editAction();
    }

    /**
     * Workaround to avoid php7 warnings of wrong type hint.
     */
    public function initializeUpdateAction(): void
    {
        if ($this->arguments->hasArgument('user')) {
            /** @var Argument $user */
            $user = $this->arguments['user'];
            $user->setDataType(\MEDIAESSENZ\FemanagerMailSubscribe\Domain\Model\User::class);
        }
    }
}
