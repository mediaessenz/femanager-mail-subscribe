<?php
declare(strict_types=1);

namespace MEDIAESSENZ\FemanagerMailSubscribe\Xclass\Extbase\Mvc\Controller;

/**
 * Class Argument
 */
class Argument extends \TYPO3\CMS\Extbase\Mvc\Controller\Argument
{
    /**
     * @param string $classname
     */
    public function setDataType(string $classname)
    {
        $this->dataType = $classname;
    }
}
