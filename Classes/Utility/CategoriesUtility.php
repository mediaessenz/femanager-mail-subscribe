<?php
namespace MEDIAESSENZ\FemanagerMailSubscribe\Utility;

use MEDIAESSENZ\FemanagerMailSubscribe\Domain\Model\Category;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\QueryResult;

class CategoriesUtility
{
    public static function filterCategories($allCategories, array $settings): array|QueryResult
    {
        if (!($settings['misc']['categories'] ?? false) && !($settings['misc']['categoryRoots'] ?? false)) {
            return $allCategories;
        }
        $filteredCategories = [];
        if ($allCategories->count() > 0) {
            if ($settings['misc']['categories'] ?? false) {
                $categoryUids = GeneralUtility::intExplode(',', $settings['misc']['categories'], true);
                /** @var Category $category */
                foreach ($allCategories as $category) {
                    if (in_array($category->getUid(), $categoryUids)) {
                        $filteredCategories[$category->getUid()] = $category;
                    }
                }
            }
            if ($settings['misc']['categoryRoots'] ?? false) {
                $categoryPids = GeneralUtility::intExplode(',', $settings['misc']['categoryRoots'], true);
                /** @var Category $category */
                foreach ($allCategories as $category) {
                    if (in_array($category->getParent()?->getUid(), $categoryPids) && !array_key_exists($category->getUid(), $filteredCategories)) {
                        $filteredCategories[$category->getUid()] = $category;
                    }
                }
            }
        }

        return $filteredCategories;
    }
}
