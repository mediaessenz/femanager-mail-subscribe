<?php

namespace MEDIAESSENZ\FemanagerMailSubscribe\Domain\Model;

use MEDIAESSENZ\FemanagerMailSubscribe\Domain\Repository\CategoryRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Class User
 */
class User extends \In2code\Femanager\Domain\Model\User
{
    protected bool $mailActive = false;
    protected bool $mailHtml = true;
    protected string $mailSalutation = '';

    /**
     * @var ObjectStorage<Category>|null
     */
    protected ?ObjectStorage $categories = null;

    public function __construct(protected string $username = '', protected string $password = '')
    {
        $this->categories = new ObjectStorage();
        parent::__construct($username, $this->password);
    }

    public function isMailActive(): bool
    {
        return $this->mailActive;
    }

    public function setMailActive($mailActive): void
    {
        $this->mailActive = (bool)$mailActive;
    }

    public function isMailHtml(): bool
    {
        return $this->mailHtml;
    }

    public function setMailHtml($mailHtml): void
    {
        $this->mailHtml = (bool)$mailHtml;
    }

    /**
     * @return string
     */
    public function getMailSalutation(): string
    {
        return $this->mailSalutation;
    }

    /**
     * @param string $mailSalutation
     */
    public function setMailSalutation(string $mailSalutation): void
    {
        $this->mailSalutation = $mailSalutation;
    }

    public function getCategories(): ?ObjectStorage
    {
        return $this->categories;
    }

    public function setCategories($categories): void
    {
        if (is_string($categories)) {
            $categoryUids = GeneralUtility::intExplode(',', $categories, true);
            $categoryRepository = GeneralUtility::makeInstance(CategoryRepository::class);
            $categories = new ObjectStorage();
            foreach ($categoryUids as $categoryUid) {
                $category = $categoryRepository->findByUid($categoryUid);
                if ($category instanceof Category) {
                    $categories->attach($category);
                }
            }
        }
        $this->categories = $categories;
    }
}
