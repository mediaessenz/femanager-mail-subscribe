.. include:: /Includes.rst.txt

.. _dependencies:

============
Dependencies
============

Femanager-mail-subscribe requires the following packages, which
all can be found at packagist.org or in the TYPO3 TER:

*  in2code/femanager (https://github.com/in2code-de/femanager)
*  mediaessenz/mail (https://gitlab.com/mediaessenz/mail)
