.. include:: /Includes.rst.txt

.. _whatDoesItDo:

================
What does it do?
================

Femanager-mail-subscribe is an extension for TYPO3 CMS to add mail subscription fields to femanager extension.

The following fields will be available in the femanager plugin:

- Newsletter subscription (Fluid: mailActive)
- HTML newsletter (Fluid: mailHtml)
- Newsletter categories (Fluid: categories)

New and existing users will be able to subscribe to a mail newsletter and select mail categories if enabled.
