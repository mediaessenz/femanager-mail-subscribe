.. include:: /Includes.rst.txt

.. _thanks:
.. _sponsoring:

==========
Sponsoring
==========

This extension and the manual took some hours to create.
If this extension helps you in any way to meet your business needs,
please consider to give something back.

Here are two ways to balance energy:

Patreon
=======

Support me on `patreon.com <https://www.patreon.com/alexandergrein>`__

PayPal
======

Support me on `paypal.com <https://www.paypal.me/AlexanderGrein/25>`__.

It's just one click away!
