.. include:: /Includes.rst.txt

.. _configuration:

=============
Configuration
=============

To add the subscription fields to femanager, a corresponding TypoScript is provided by
this extension.

Goto module :guilabel:`Web > Template` and chose the same page where you have included the
TypoScript of femanager.

Switch to tab :guilabel:`Includes` and add (after the femanager template) the following
template from the list to the right:

*  :guilabel:`femanager mail subscription (femanager_mail_subscribe)`

.. include:: /Images/IncludeTypoScriptTemplate.rst.txt

If you want to add your own fluid partials for the subscription field, you can change the
corresponding TypoScript constant (plugin.tx_femanagermailsubscribe.view.partialRootPath)
or just add your own TypoScript path to the plugin.tx_femanager.view.partialRootPaths array.

Read more about possible configurations via TypoScript in the :ref:`Reference <typoscript-settings>` section.
