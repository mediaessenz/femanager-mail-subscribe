.. include:: /Includes.rst.txt

.. _installation:

============
Installation
============

To add this extension to your existing TYPO3 system, you have tree options:

#. Composer installation (recommended)

   .. code-block:: bash

      composer require mediaessenz/femanager-mail-subscribe

#. Extension manager

   Open "Admin and maintenance tools" > "TYPO3 extension manager" and search for "femanager_mail_subscribe".
   Install it by clicking on the install button.

#. Download

   Download it from here: https://extensions.typo3.org/extension/femanager_mail_subscribe and use the
   extension manager to upload and install it.


Clear all caches
----------------

Clearing all caches after installing a new extension is always a good idea.

Since this extension x-classes the \TYPO3\CMS\Extbase\Mvc\Controller\Argument class and also extends
some femanager classes (NewController, EditController, User-Model) it is also necessary to clear the
cache inside the install tool!

.. include:: /Images/FlushCache.rst.txt
