.. include:: /Includes.rst.txt

.. _quick-start:

===========
Quick start
===========

.. rst-class:: bignums-tip

#. :ref:`Installation <installation>`

#. :ref:`Configuration <configuration>`

.. toctree::
   :maxdepth: 5
   :titlesonly:
   :hidden:

   Installation
   Configuration
