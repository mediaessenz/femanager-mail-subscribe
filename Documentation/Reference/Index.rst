.. include:: /Includes.rst.txt

.. highlight:: typoscript

.. _typoscript-settings:

====================
TypoScript Constants
====================

.. _ts-view-template-root-path:

.. confval:: templateRootPath

   :type: string
   :Default: EXT:femanager_mail_subscribe/Resources/Private/Templates/
   :Path: plugin.tx_femanagermailsubscribe.view.templateRootPath

   Path to template root for femanager_mail_subscribe

.. _ts-view-partial-root-path:

.. confval:: partialRootPath

   :type: string
   :Default: EXT:femanager_mail_subscribe/Resources/Private/Partials/
   :Path: plugin.tx_femanagermailsubscribe.view.partialRootPath

   Path to partial root for femanager_mail_subscribe

.. _ts-view-layout-root-path:

.. confval:: layoutRootPath

   :type: string
   :Default: EXT:femanager_mail_subscribe/Resources/Private/Layouts/
   :Path: plugin.tx_femanagermailsubscribe.view.layoutRootPath

   Path to layout root for femanager_mail_subscribe

.. _ts-categories:

.. confval:: categories

   :type: string
   :Default:
   :Path: plugin.tx_femanagermailsubscribe.settings.categories

   Restrict the selectable categories by a comma separated list of its uids

.. _ts-category-roots:

.. confval:: categoryRoots

   :type: string
   :Default:
   :Path: plugin.tx_femanagermailsubscribe.settings.categoryRoots

   Restrict the selectable categories by a comma separated list of its parent
   category uids


.. _typoscript-setup:

================
TypoScript Setup
================

.. _typoscript-change-language-labels:

Change language labels
======================

If you want to add translated language labels, simply add the following
TypoScript setup to your TypoScript template (example in german below)

.. code-block:: typoscript

   plugin.tx_femanagermailsubscribe {
     _LOCAL_LANG.de {
       label.mailActive = Für Newsletter anmelden
       label.mailHtml = HTML E-Mails empfangen?
       label.categories = Newsletter Kategorien
     }
   }
