.. include:: /Includes.rst.txt

===========================
Femanager Mail Subscription
===========================

:Extension key:
   femanager_mail_subscribe

:Package name:
   mediaessenz/femanager-mail-subscribe

:Version:
   |release|

:Language:
   en

:Author:
   Alexander Grein

:License:
   This document is published under the
   `Open Publication License <https://www.opencontent.org/openpub/>`__.

:Rendered:
   |today|

----

This extension add mail subscription fields to femanager

----

.. container:: row m-0 p-0

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Quick start <quick-start>`

         .. container:: card-body

            A quick introduction in how to use this extension.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Introduction <introduction>`

         .. container:: card-body

            Introduction to the femanager_mail_subscribe extension,
            dependencies, general information.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`TypoScript settings <typoscript-settings>`

         .. container:: card-body

            TypoScript reference of this extension.


.. Table of Contents

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :hidden:

   Introduction/Index
   QuickStart/Index
   Reference/Index

.. Meta Menu

.. toctree::
   :hidden:

   Sitemap
   genindex
