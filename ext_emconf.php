<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'femanager mail subscription',
    'description' => 'Adds newsletter subscription fields for mail extension to femanager',
    'category' => 'plugin',
    'author' => 'Alexander Grein',
    'author_email' => 'alexander.grein@gmail.com',
    'author_company' => 'MEDIA::ESSENZ',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => false,
    'createDirs' => '',
    'clearCacheOnLoad' => true,
    'version' => '3.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '12.4.0-13.4.99',
            'femanager' => '8.0.0-13.99.99',
            'mail' => '2.1.0-3.99.99'
        ],
        'conflicts' => [],
        'suggests' => [],
    ]
];
